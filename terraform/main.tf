#Configuração provider
provider "aws" { #provedor AWS
    access_key = "CHAVE" #chave
    secret_key = "CHAVE" #chave
    region = "${var.region}" #variavel região
}

#Configuração auto scaling group
resource "aws_autoscaling_group" "scalegroup" { #auto scaling
    launch_configuration = "${aws_launch_configuration.sre.name}" #launch (variavel)
    availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"] #zonas disponiveis
    min_size = 3 #minimo
    max_size = 4 #maximo
    enabled_metrics = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupTotalInstances"] # metricas
    metrics_granularity="1Minute" #metricas
    load_balancers= ["${aws_elb.elb1.id}"] #load
    health_check_type="ELB" #health
    tag {
        key = "Name" 
        value = "sre-server"
        propagate_at_launch = true
    }
}

#configuração regra sc ssh
resource "aws_security_group_rule" "ssh" { #sc ssh
    security_group_id = "${aws_security_group.websg.id}" #variavel sc
    type = "ingress" #tipo
    from_port = 22 #porta
    to_port = 22 #porta
    protocol = "tcp" #tipo de conexao
    cidr_blocks = ["0.0.0.0/0"] 
}

#Configuração launch (instancia sendo criada via escalonamento)
resource "aws_launch_configuration" "sre" { #configuração launch
    image_id=  "${var.ami}" # (variavel) imagem-id
    instance_type = "t2.micro" #tipo instancia (variavel)
    security_groups = ["${aws_security_group.websg.id}"] #sc (variavel)
    key_name = "${aws_key_pair.myawskeypair.key_name}" #chave (variavel)

    lifecycle {
        create_before_destroy = true
    }
}

#Configuração key
resource "aws_key_pair" "myawskeypair" { #chave
    key_name = "myawskeypair" #nome_chave
    public_key = "${file("${var.key_path}")}" #chave publica (caminho da minha chave configurado nas variaveis)
}

#configuração auto scaling policy
resource "aws_autoscaling_policy" "autopolicy" {
    name = "terraform-autoplicy"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.scalegroup.name}"
}

#Configuração alarme
resource "aws_cloudwatch_metric_alarm" "cpualarm" { #tipo de alarme (cpu)
    alarm_name = "alarm-cpu" #nome do alarme
    comparison_operator = "GreaterThanOrEqualToThreshold" #condição
    evaluation_periods = "2" #periodo
    metric_name = "CPUUtilization" #nome da metrica
    namespace = "AWS/EC2" #meu name space
    period = "120" #periodo
    statistic = "Average" # por media
    threshold = "60" # "limite para alarme"

    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.scalegroup.name}" #variavel auto scaling
    }

    alarm_description = "This metric monitor EC2 instance cpu utilization" #descrição
    alarm_actions = ["${aws_autoscaling_policy.autopolicy.arn}"] #ação (variavel "policy")
}

#configuração auto scaling policy down
resource "aws_autoscaling_policy" "autopolicy-down" { #auto scaling policy
    name = "autoplicy-down" #nome
    scaling_adjustment = -1 #ajuste
    adjustment_type = "ChangeInCapacity" #tipo
    cooldown = 300 
    autoscaling_group_name = "${aws_autoscaling_group.scalegroup.name}" #variavel nome
}

#configuraço alarme cpu down
resource "aws_cloudwatch_metric_alarm" "cpualarm-down" { #metrica alarme
    alarm_name = "alarm-cpu" #nome do alarme
    comparison_operator = "LessThanOrEqualToThreshold" #condição
    evaluation_periods = "2" #periodo
    metric_name = "CPUUtilization" #utilização de cpu 'nome metrica'
    namespace = "AWS/EC2" # meu name space
    period = "120" #periodo
    statistic = "Average" #tipo "media"
    threshold = "10" #limite de alarme

    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.scalegroup.name}" #variavel nome auto scaling
    }

    alarm_description = "This metric monitor EC2 instance cpu utilization" #descrição alarme
    alarm_actions = ["${aws_autoscaling_policy.autopolicy-down.arn}"] #variavel alarme
}

#configuração sc web
resource "aws_security_group" "websg" { #sc resource
    name = "security_group_for_web_server" #nome do sc
    ingress {
        from_port = 80 #porta
        to_port = 80 #porta
        protocol = "tcp" #tipo de conexao
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    lifecycle {
        create_before_destroy = true
    }
}

#configuração sc load
resource "aws_security_group" "elbsg" { #sc load com elbsg
    name = "security_group_for_elb" #nome sc
    ingress {
        from_port = 80 #porta
        to_port = 80 #porta
        protocol = "tcp" #tipo de conexao
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    lifecycle {
        create_before_destroy = true
    }
}

#configuração load
resource "aws_elb" "elb1" { #load 
    name = "terraform-elb" #nome
    availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"] #areas "zonas"
    security_groups = ["${aws_security_group.elbsg.id}"] #sc variavel

    listener {
        instance_port = 80 #porta
        instance_protocol = "http" #protocolo
        lb_port = 80 #porta load
        lb_protocol = "http" #protocolo load
    }

    health_check {
        healthy_threshold = 2 
        unhealthy_threshold = 2
        timeout = 3
        target = "HTTP:80/"
        interval = 30
    }

    cross_zone_load_balancing = true
    idle_timeout = 400
    connection_draining = true
    connection_draining_timeout = 400

    tags = {
        Name = "terraform-elb"
    }
}

resource "aws_lb_cookie_stickiness_policy" "cookie_stickness" {
    name = "cookiestickness"
    load_balancer = "${aws_elb.elb1.id}" #variavel load
    lb_port = 80 #porta
    cookie_expiration_period = 600
}

output "availabilityzones" { #nome
    value = ["us-east-1a", "us-east-1b", "us-east-1c"] #zonas
}

output "elb-dns" {
    value = "${aws_elb.elb1.dns_name}" #variavel load dns
}