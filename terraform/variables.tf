variable "region" {
  description = "AWS Region"
  default = "us-east-1"
}

variable "key_path" {
  description = "Public key path"
  default = "/home/wleite/.ssh/id_rsa.pub"
}

variable "ami" {
  description = "AMI"
  default = "ami-0ac80df6eff0e70b5" #ubuntu
}

variable "instance_type" {
  description = "EC2 instance type"
  default = "t2.micro"
}
